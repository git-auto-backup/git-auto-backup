# git-auto-backup

Script to automatically pull a list of repositories for backup purposes.

## How it works

This repository contains the scripts and all configuration files.
To set up, you need to **fork**, edit the configuration files, and run a few bash commands on the backup machine.
After initial setup, you can edit the configuration files (and even the scripts) whenever you like on any machine *other than a backup machine*, push the edits and the new configuration will be automatically used on the next but one scheduled backup run.

## Installation

> Why is it necessary to fork & clone rather than just clone?
>
> Because if you just clone then you will not be able to edit the configuration files remotely, which makes backing up a chore.
> You would also have to set `pull: false` for your clone of this repository, or your configuration files are likely to be corrupted or overwritten.

> This assumes that you want all your repositories backed up to subdirectories of `/mnt/backup/all-repos/`, this directory already exists.
> If you want to backup elsewhere, you should edit the lines below accordingly.
> It is assumed this directory already exists and the user who will set up & run the backup has write access.

If you do not already have one, create a GitLab user and [add your public key](https://docs.gitlab.com/ee/ssh/).

**Fork** this repository by clicking on the Fork icon in the top right and choosing your own GitLab namespace.
In the main GitLab page of your own fork of this repository, click Clone at the top right and note the SSH URL.

Clone your fork of this repository:
```bash
cd /mnt/backup/all-repos
mkdir git-auto-backup
cd git-auto-backup
git clone git@gitlab.com:dazsmith/git-auto-backup.git
```
where the URL `git@gitlab.com:dazsmith/git-auto-backup.git` should be replaced by the SSH URL of your own fork of this repository (as noted above).

Create a python3 environment:
```bash
sudo apt update
sudo apt install virtualenv
cd ~
mkdir pyenv
cd pyenv
virtualenv -p python3 env-git-auto-backup
```

Set up the python3 environment for use with git-auto-backup:
```bash
source ~/pyenv/env-git-auto-backup/bin/activate
pip install -U pip setuptools wheel
pip install ruamel.yaml
deactivate
```

## Configuration

### To make the script run

You have to edit the configuration files to point to the right repositories.
Most importantly:
* line 9 of `config.yaml` must contain the URL of your fork of this repository (as noted above).
* line 4 of `config-backupserver.yaml` must be `/mnt/backup/all-repos/` or your choice of its replacement, as described at the top of the Installation section.

### Backing up your own repositories

You are still not configured to back up your own repositories, just the example repositories (which are not very interesting) and your fork of this repository.
For each repository of your own that you want backed up, you need to put some information about the repository in each of `config.yaml` and `config-backupserver.yaml`.
See the config file specifications below for details on how to do this.

### Working with more than one backup machine

This is easy!
Just create a new machine configuration file (like `config-backupserver2.yaml`) for your second machine.
The idea is to reference the same master configuration file `config.yaml` and choose to back up the same or different repositories from the first machine by listing different repositories or inserting `enabled: false` lines in appropriate places.
But, if you prefer, the second machine could also use its own master configuration file.

## Running the script

Enable the virtual environment and run the python script:
```bash
source ~/pyenv/env-git-auto-backup/bin/activate
cd /mnt/backup/all-repos/git-auto-backup/git-auto-backup
python3 git-auto-backup.py -c config.yaml -m config-backupserver.yaml
deactivate
```
Alternatively, you can use the bash script to do all this for you:
```bash
bash git-auto-backup.sh -m config-backupserver.yaml
```
The bash script assumes that the master config file is called `config.yaml`, and both config files are stored in the same directory as the bash script itself.

## Automating backup
This can be done with a cron job.

If you do not completely trust your remotes, you might also want to use rsnapshot to take snapshots of your backed up repositories after each run of git-auto-backup.

## Config file specifications

To back up to $`n`$ machines, you need $`n+1`$ configuration files, because you need a master configuration file, plus one configuration file for each machine.
The configuration files are all written in yaml 1.2.

### Master config file

The repos are arranged in categories.
Each category contains a list of repos.
Each category has:
* `name` a string
* `repolist` a list of repos

Each repo has
* `name` a string
* `remote` a string for SSH or HTTPS clone, fetch & pull commands

There is also a `globalsettings` top level key, which is currently unused.

#### Example master config file

```yaml
%YAML 1.2
--- # Master config file for git-auto-backup
globalsettings:
  placeholder: false
repocategories:
  - name: git-auto-backup
    repolist:
      - name: git-auto-backup
        remote: git@gitlab.com:git-auto-backup/git-auto-backup.git
  - name: example-category-1
    repolist:
      - name: repo-1-1
        remote: git@gitlab.com:git-auto-backup/repo-1-1.git
      - name: repo-1-2
        remote: git@gitlab.com:git-auto-backup/repo-1-2.git
  - name: example-category-2
    repolist:
      - name: repo-2-1
        remote: git@gitlab.com:git-auto-backup/repo-2-1.git
      - name: repo-2-2
        remote: git@gitlab.com:git-auto-backup/repo-2-2.git
```

> Don't forget to **edit line 9** to the URL for the remote of **your fork** of this repository.

### Machine config file

The `globalsettings` key provides
* `localdir` the directory into which all repos will be backed up.

Under `repocategories`, the repos are arranged in categories.
Each category contains a list of repos.
Each category has:
* `name` a string which should match exactly one of the corresponding entries in the master config file
* `enabled` boolean default `true`; set `false` to ignore all repos in this category
* `localdir` a string default `name` which is the subdirectory of the `globalsettings` `localdir` into which all repos of this category will be saved
* `repolist` a list of repos

Each repo has
* `name` a string which should match exactly one of the corresponding entries in the corresponding category of the master config file
* `enabled` boolean default `true`; set `false` to ignore this repo
* `localdir` a string default `name` which is the subdirectory of the repocategory `localdir` into which all this repo will be saved
* `createifmissing` boolean default `true`; set `false` so that this repo will not be cloned if it has not previously been cloned
* `pull` boolean default `false`; set `true` so that this repo will be pulled rather than just fetched

> Note that `pull` should **usually** be set true on the backup of (your own fork of) this repo so that it will get updates to the program and config files.
> The only exception is in a testing environment where you may be making unpushed (or, worse, uncommitted) edits to the configuration or scripts.

#### Example machine config file

```yaml
%YAML 1.2
--- # Config file for git-auto-backup for use with backupserver
globalsettings:
  localdir: /mnt/backup/all-repos/
repocategories:
  - name: git-auto-backup
    enabled: true
    localdir: git-auto-backup/
    repolist:
      - name: git-auto-backup
        pull: true
  - name: example-category-1
    enabled: true
    localdir: example-category-1/
    repolist:
      - name: repo-1-1
      - name: repo-1-2
        enabled: false
  - name: example-category-2
    enabled: true
    localdir: example-category-2/
    repolist:
      - name: repo-2-1
      - name: repo-2-2
        localdir: prefered-name-for-repo-2-2
        createifmissing: false
```
> Don't forget to **edit line 4** to `localdir: x` so that `x/git-auto-backup/git-auto-backup` is the local directory containing **your clone of your fork** of this repository.

## Contributions

Please file bug reports & feature requests as GitLab issues.
