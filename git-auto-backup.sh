#!/bin/bash

set -e
set -u
set -o pipefail

while getopts 'm:' OPTION; do
  case "$OPTION" in
    m)
      machineconfigfile="$OPTARG"
      echo "Using machine config file $OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-m config-backupserver.yaml]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

VENVPATH=~/pyenv/env-git-auto-backup
PYTHONSCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

set +u
source $VENVPATH/bin/activate
set -u

cd $PYTHONSCRIPTPATH
python3 git-auto-backup.py -c config.yaml -m "$machineconfigfile"

deactivate
