#!/usr/bin/env python3

# argparse is used to parse command line arguments
from argparse import ArgumentParser
# YAML is used to load the config file
from ruamel.yaml import YAML
# subprocess is used to call git
import subprocess
# os is used to check if a git repository already exists at the expected local path
import os
# shutil is used to delete a directory which may not be empty
import shutil

def fnEndSlash(i): # returns a string with appended '/' if necessary
    o = i
    if not i[-1:] == '/':
        o = o + '/'
    return o

def fnIsGitDirectory(path = '.'): # returns true if there is a git repository at path
    return subprocess.call(['git -C ' + path + ' status'], shell=True, stderr=subprocess.STDOUT, stdout = open(os.devnull, 'w')) == 0

def fnDefaultIfMissing(dictry,key,defval):
    try:
        o = dictry[key]
    except:
        o = defval
    return o

def fnRunShellCmd(shellcmd = '', verbose = True):
    if verbose:
        print('running command: ' + shellcmd)
    # process = subprocess.Popen([shellcmd], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # stdout, stderr = process.communicate()
    # print(stdout)
    # print(stderr)
    result = subprocess.check_output([shellcmd], shell=True, stderr=subprocess.STDOUT, universal_newlines=True).strip()
    # print(result)
    return result

# Load the command line arguments
parser = ArgumentParser()
parser.add_argument('-m', '--machine', dest='machine',
                    default='other',
                    help='config file for current machine eg config-testingserver.yaml')
parser.add_argument('-c', '--config', dest='configfilename',
                    default='config.yaml',
                    help='master config file eg config.yaml')
args = vars(parser.parse_args())
machine = args['machine']

# Load the global settings and list of repos from the config file, default: config.yaml
# and also from the machine's config file
yaml=YAML(typ='safe')
configfile = open(args['configfilename'])
configdata = yaml.load(configfile)
masterglobalsettings = configdata['globalsettings']
masterrepocategories = configdata['repocategories']
configfile.close()

configfile = open(args['machine'])
configdata = yaml.load(configfile)
machineglobalsettings = configdata['globalsettings']
machinerepocategories = configdata['repocategories']
configfile.close()

localdir = fnEndSlash(machineglobalsettings['localdir'])

# Process config dictionaries, inserting default values wherever data are missing
for category in machinerepocategories:
    category['enabled'] = fnDefaultIfMissing(category,'enabled',True)
    category['localdir'] = fnDefaultIfMissing(category,'localdir',category['name'] + '-repos/')
    # check master configuration is valid
    mastercat = list(filter(lambda mcat: mcat['name'] == category['name'], masterrepocategories))[0]
    if category['enabled']:
        print(f"Repo category {category['name']} is enabled for this machine; running.")
        categorylocaldir = localdir + fnEndSlash(category['localdir'])
        for repo in category['repolist']:
            repo['name'] = fnDefaultIfMissing(repo,'name','')
            if repo['name'] == '':
                print(f"Config file {machine} specifies a repo with no name; skipping.")
            else:
                # check machine configuration is valid
                repo['enabled'] = fnDefaultIfMissing(repo,'enabled',True)
                repo['pull'] = fnDefaultIfMissing(repo,'pull',False)
                repo['localdir'] = fnDefaultIfMissing(repo,'localdir',repo['name'] + '/')
                repo['createifmissing'] = fnDefaultIfMissing(repo,'createifmissing',True)
                # check master configuration is valid
                masterrep = list(filter(lambda mrep: mrep['name'] == repo['name'], mastercat['repolist']))[0]
                masterrep['remote'] = fnDefaultIfMissing(masterrep,'remote','')
                if masterrep['remote'] == '':
                    print(f"Repo {repo['name']} does not have a remote listed in master config file {args['configfilename']}; skipping.")
                elif repo['enabled']:
                    print(f"Repo {repo['name']} is enabled for this machine; running.")
                    repolocaldir = categorylocaldir + fnEndSlash(repo['localdir'])
                    # check whether the remote has changed; if it has, then delete the local repository completely and start again with a fresh clone
                    if fnIsGitDirectory(repolocaldir):
                        shellcmd = 'cd "' + repolocaldir + '"; git remote get-url origin'
                        existingremote = fnRunShellCmd(shellcmd,False)
                        if repo['createifmissing'] and not existingremote == masterrep['remote']:
                            # delete the local repository completely
                            print(f"The existing repository has remote {existingremote}. The master config file suggests the remote should be {masterrep['remote']}, which is different. Deleting existing repository, so that it can be recloned.")
                            shutil.rmtree(repolocaldir)
                    # check whether a local repository exists; if not, clone it; if it already exists then fetch it, or pull if pull is set to true.
                    if repo['createifmissing'] and not fnIsGitDirectory(repolocaldir):
                        shellcmd = 'git clone ' + masterrep['remote'] + ' ' + repolocaldir
                    elif repo['pull']:
                        shellcmd = 'cd "' + repolocaldir + '"; git pull'
                    else:
                        shellcmd = 'cd "' + repolocaldir + '"; git fetch'
                    stdout = fnRunShellCmd(shellcmd)
                else:
                    print(f"Repo {repo['name']} is disabled for this machine; skipping.")
    else:
        print(f"Repo category {category['name']} is disabled for this machine; skipping.")
